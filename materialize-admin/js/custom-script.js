/*================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 4.0
	Author: PIXINVENT
	Author URL: https://themeforest.net/user/pixinvent/portfolio
================================================================================

NOTE:
------
PLACE HERE YOUR OWN JS CODES AND IF NEEDED.
WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR CUSTOM SCRIPT IT'S BETTER LIKE THIS. */

// Bar chart ( New Clients)


//GAUGE CHART
new Chartist.Pie('#finance-sales-chard', {
	series: [40, 60]
  }, {
	donut: true,
	donutWidth: 40,
	startAngle: 270,
	total: 200,
	showLabel: false
  });

  new Chartist.Pie('#finance-operating-chard', {
	series: [60, 40]
  }, {
	donut: true,
	donutWidth: 40,
	startAngle: 270,
	total: 200,
	showLabel: false
  });

  new Chartist.Pie('#finance-income-chard', {
	series: [80, 20]
  }, {
	donut: true,
	donutWidth: 40,
	startAngle: 270,
	total: 200,
	showLabel: false
  });
  new Chartist.Pie('#finance-net-chard', {
	series: [70, 30]
  }, {
	donut: true,
	donutWidth: 40,
	startAngle: 270,
	total: 200,
	showLabel: false
  });
